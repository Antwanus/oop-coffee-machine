from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

menu = Menu()
coffee_maker = CoffeeMaker()
money_machine = MoneyMachine()

menu_item_latte = menu.find_drink("latte")
price = menu_item_latte.cost

while True:
    choice_prompt = input(f"Please choose your beverage /{menu.get_items()}:\t")
    if choice_prompt == 'report':
        coffee_maker.report()
    else:
        choice = menu.find_drink(choice_prompt)
        # CHECK RESOURCES
        if coffee_maker.is_resource_sufficient(choice):
            # print("DEBUG: resources are sufficient for", choice.name)
            # PROCESS PAYMENT
            is_tx_success = money_machine.make_payment(choice.cost)
            # print("DEBUG: payment completed")
            # MAKE COFFEE
            if is_tx_success:
                coffee_maker.make_coffee(choice)
                # print("DEBUG: tx success")
                coffee_maker.report()
            # else:
                # print("DEBUG: tx failed")
        else:
            print('Required resources are not present for', choice.name)
